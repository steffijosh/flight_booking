package com.demo.erukekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ErukekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErukekaServerApplication.class, args);
	}

}
