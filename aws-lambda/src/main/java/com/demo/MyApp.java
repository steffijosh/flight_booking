package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MyApp {
	
	@Bean
	public RestTemplate restTempleate() {
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(MyApp.class, args);
	}
	
}
