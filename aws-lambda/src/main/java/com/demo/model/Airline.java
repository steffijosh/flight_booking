package com.demo.model;

import java.util.List;





public class Airline {


	private Long id;
	private String airlineName;
	private String address;
	private String contactNumber;
	// private String parts;
	private List<Flight> flight;

	public Airline() {
		
	}

	public Airline(Long id, String airlineName, String address, String contactNumber, List<Flight> flight) {
		
		this.id = id;
		this.airlineName = airlineName;
		this.address = address;
		this.contactNumber = contactNumber;
		this.flight = flight;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public List<Flight> getFlight() {
		return flight;
	}

	
}