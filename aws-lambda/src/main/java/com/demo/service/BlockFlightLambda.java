package com.demo.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.function.Function;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.demo.model.Airline;
import com.demo.model.Flight;

@Service
public class BlockFlightLambda {

	public List<Flight> blockedFlights = null;

	/*
	 * @Autowired private FlightDao flightDao;
	 */
	List<Object> allFlights = new ArrayList<Object>();

	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://flight-booking.cp2udssjtiyd.us-east-2.rds.amazonaws.com:3306/flight_booking_rds";
	// Database credentials
	static final String USER = "admin";
	static final String PASS = "8OMDXmt162hkVhqNwTdN";
	Connection conn = null;
	java.sql.Statement stmt = null;
	ResultSet rs = null;
	Vector<String> vector = new Vector<String>();
	List<Flight> flightList = new ArrayList<Flight>();
	

	/*
	 * @PostConstruct public List<Flight> getFlights() throws RuntimeException { try
	 * {
	 * 
	 * this.blockedFlights = flightDao.findAll(); // TODO Auto-generated method stub
	 * System.out.println(this.blockedFlights); return blockedFlights;
	 * 
	 * } catch (Exception e) { e.printStackTrace(); throw new RuntimeException(e); }
	 * 
	 * }
	 */
	/*
	 * @Bean public Function<APIGatewayProxyRequestEvent,
	 * APIGatewayProxyResponseEvent> getAllBooks() throws ClassNotFoundException {
	 * System.out.println(this.blockedFlights);
	 * Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> f =
	 * (event) -> { return new
	 * APIGatewayProxyResponseEvent().withBody(blockedFlights.toString()).
	 * withStatusCode(200); }; return f; }
	 */

	@Bean
	public Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> getblockedflights()
			throws ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> f = (event) -> {
			String responseMessage = null;
			try {

				conn = DriverManager.getConnection(DB_URL, USER, PASS);
				stmt = conn.createStatement();
			
				String sql;
				sql = "SELECT * from flight where active";
				rs = stmt.executeQuery(sql);
				while (rs.next()) {
					Flight flight = new Flight();
					Airline airline = new Airline();
					flight.setId(rs.getLong("id"));
					flight.setFrom(rs.getString("from_location"));
				    flight.setFlightNumber(rs.getString("flight_number"));
				    flight.setTo(rs.getString("to_location"));
				    flight.setScheduledDates(rs.getString("scheduled_dates"));
				    flight.setMealType(rs.getString("meal_type"));
				    flight.setCost(rs.getInt("cost"));
				    flight.setActive(rs.getBoolean("active"));
				    flight.setAirlineId(rs.getInt("airline_id"));
					flightList.add(flight);
					
				}
				System.out.println("!!!!!!!!!!!!!!!!!!1" + flightList);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
				} catch (SQLException e) {
				
					e.printStackTrace();
				}
			}
			System.out.println("getting inside");

			return new APIGatewayProxyResponseEvent().withBody(flightList.toString()).withStatusCode(200);
		};
		return f;
	}

}