package com.demo.flightBooking;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.demo.flightBooking.controller.AdminController;
import com.demo.flightBooking.dao.AirlineDao;
import com.demo.flightBooking.dao.DiscountDao;
import com.demo.flightBooking.dao.FlightDao;
import com.demo.flightBooking.entities.Airline;
import com.demo.flightBooking.entities.Flight;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;


@SpringBootTest
class FlightBookingApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Autowired
	private AdminController AdminController;

	@MockBean
	private AirlineDao airlineRepo;
	@MockBean
	private DiscountDao discountRepo;
	@MockBean
	private FlightDao flightRepo;
	
	@Test
	public void testAddAirline() {
		Airline Airline = new Airline();
		Airline.setAddress("abc");
		Airline.setAirlineName("airline567");
		Airline.setContactNumber("44564574574");
		Mockito.when(airlineRepo.save(Airline)).thenReturn(Airline);
		Airline Airline1 = this.AdminController.addAirline(Airline);
		Assertions.assertEquals(Airline, Airline1);
	}
	
	
	@Test
	public void testGetByAirlineName() {
		String name="GoAir";
		Airline Airline = new Airline();
		Mockito.when(this.airlineRepo.findByAirlineName(name)).thenReturn(Airline);
		Airline Airline1 = AdminController.getByAirlineName(name);
		Assertions.assertSame(Airline1, Airline);
	}

	@Test
	public void testGetAllFlights() {
		Mockito.when(this.flightRepo.findAll()).thenReturn(Arrays.asList(new Flight(), new Flight()));
		List<Flight> flights = AdminController.getAllFlights();
		Assertions.assertSame(2, flights.size());
	}
}
