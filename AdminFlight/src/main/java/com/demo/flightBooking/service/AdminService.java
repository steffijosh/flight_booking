package com.demo.flightBooking.service;

import java.util.List;

import com.demo.flightBooking.entities.Airline;
import com.demo.flightBooking.entities.Booking;
import com.demo.flightBooking.entities.Discount;
import com.demo.flightBooking.entities.Flight;

public interface AdminService {

	Airline addAirline(Airline airline);

	List<Airline> getAllAirline();


	Airline getAirlineById(Long id);

	Airline getAirlineByName(String airlineName);

	Flight getByFlightId(String flightId);

	List<Flight> searchByfromAndToLocation(String from, String to);

	Flight addFlight(Flight flight);

	Flight updateFlight(Flight flightTemp);

	void deleteFlight(Long id);

	List<Discount> getAllDiscount();

	Discount getDiscountByCode(String discountCode);

	Discount updateDiscount(Discount discount);

	Discount createDiscount(Discount discount);

	void deleteDiscount(Long id);

	List<Flight> getAllFlights();

	List<Flight> getBlockedFlights(Long id);

	List<Flight> updateBlockedFlight(List<Flight> blockedFlights);

	Discount getDiscountByID(Long id);

	List<Airline> getAllAirlines();

	Airline getAirlineByID(Long id);

	Airline updateAirline(Airline airline);

	Flight getFlightByID(Long id);

	List<Flight> getFlightByAirlineName(Long id);

	List<Booking> getBlockedBooking(Long id);

	List<Booking> updateBlockedBooking(List<Booking> tempBooking);
	
	

}
