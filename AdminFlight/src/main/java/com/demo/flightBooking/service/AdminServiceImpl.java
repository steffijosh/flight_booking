package com.demo.flightBooking.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.demo.flightBooking.dao.AirlineDao;
import com.demo.flightBooking.dao.BookingDao;
import com.demo.flightBooking.dao.DiscountDao;
import com.demo.flightBooking.dao.FlightDao;
import com.demo.flightBooking.entities.Airline;
import com.demo.flightBooking.entities.Booking;
import com.demo.flightBooking.entities.Discount;
import com.demo.flightBooking.entities.Flight;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AirlineDao airlineDao;

	@Autowired
	private FlightDao flightDao;

	@Autowired
	private DiscountDao discountDao;
	
	@Autowired
	private BookingDao bookingDao;

	@Override
	public Airline addAirline(Airline airline) {
		// TODO Auto-generated method stub
		return airlineDao.save(airline);
	}

	@Override
	public List<Airline> getAllAirline() {
		// TODO Auto-generated method stub
		Iterable<Airline> findAllFlight = airlineDao.findAll();
		return (List<Airline>) findAllFlight;
	}

	@Override
	public List<Flight> getBlockedFlights(Long id) {
		// TODO Auto-generated method stub
		return flightDao.findByAirlineId(id);
	}

	@Override
	@Cacheable(value = "airlineById", key="#id")
	public Airline getAirlineById(Long id) {

		Optional<Airline> findByAirportCode = airlineDao.findById(id);

		if (findByAirportCode.isPresent()) {
			return findByAirportCode.get();
		} else {
			throw new RuntimeException();
		}
	}

	@Override
	public Airline getAirlineByName(String airlineName) {
		// TODO Auto-generated method stub
		Airline findAllFlight = airlineDao.findByAirlineName(airlineName);
		return findAllFlight;
	}

	@Override
	public Flight getByFlightId(String flightId) {
		// TODO Auto-generated method stub
		Optional<Flight> findByFlightId = flightDao.findByFlightNumber(flightId);

		if (findByFlightId.isPresent()) {
			return findByFlightId.get();
		} else {
			throw new RuntimeException();
		}
	}

	@Override
	public List<Flight> searchByfromAndToLocation(String from, String to) {
		// TODO Auto-generated method stub
		Iterable<Flight> findByFromAndTo = flightDao.findByFromAndTo(from, to);
		return (List<Flight>) findByFromAndTo;
	}

	@Override
	public Flight addFlight(Flight flight) {
		// TODO Auto-generated method stub
		return flightDao.save(flight);
	}

	@Override
	public Flight updateFlight(Flight flightTemp) {
		// TODO Auto-generated method stub
		return flightDao.save(flightTemp);
	}

	@Override
	public void deleteFlight(Long id) {
		// TODO Auto-generated method stub
		flightDao.deleteById(id);
	}

	@Override
	public List<Discount> getAllDiscount() {
		return discountDao.findAll();
	}

	@Override
	public Discount getDiscountByCode(String discountCode) {
		// TODO Auto-generated method stub
		return discountDao.findByDiscountCode(discountCode);
	}

	@Override
	public Discount updateDiscount(Discount discount) {

		return discountDao.save(discount);
	}

	@Override
	public Discount createDiscount(Discount discount) {
		// TODO Auto-generated method stub
		return discountDao.save(discount);
	}

	@Override
	public void deleteDiscount(Long id) {
		// TODO Auto-generated method stub
		discountDao.deleteById(id);
	}

	@Override
	public List<Flight> updateBlockedFlight(List<Flight> savedFlights) {
		// TODO Auto-generated method stub
		return flightDao.saveAll(savedFlights);
	}

	@Override
	public List<Flight> getAllFlights() {
		// TODO Auto-generated method stub
		return flightDao.findAll();
	}

	@Override
	public Discount getDiscountByID(Long id) {
		// TODO Auto-generated method stub
		Optional<Discount> findByDiscountId = discountDao.findById(id);

		if (findByDiscountId.isPresent()) {
			return findByDiscountId.get();
		} else {
			throw new RuntimeException();
		}
		
	}

	@Override
	public List<Airline> getAllAirlines() {
		// TODO Auto-generated method stub
		return airlineDao.findAll();
	}

	@Override
	public Airline getAirlineByID(Long id) {
		// TODO Auto-generated method stub
		Optional<Airline> findByAirlineId = airlineDao.findById(id);

		if (findByAirlineId.isPresent()) {
			return findByAirlineId.get();
		} else {
			throw new RuntimeException();
		}
	}

	@Override
	public Airline updateAirline(Airline airline) {
		// TODO Auto-generated method stub
		return airlineDao.save(airline);
	}

	@Override
	public Flight getFlightByID(Long id) {
		// TODO Auto-generated method stub
		Optional<Flight> findByFlightId = flightDao.findById(id);

		if (findByFlightId.isPresent()) {
			return findByFlightId.get();
		} else {
			throw new RuntimeException();
		}

	}

	@Override
	public List<Flight> getFlightByAirlineName(Long id) {
		// TODO Auto-generated method stub
		return flightDao.findByAirlineId(id);
	}

	@Override
	public List<Booking> getBlockedBooking(Long id) {
		// TODO Auto-generated method stub
		return bookingDao.findByFlightId(id);
	}

	@Override
	public List<Booking> updateBlockedBooking(List<Booking> tempBooking) {
		// TODO Auto-generated method stub
		return bookingDao.saveAll(tempBooking);
	}

}
