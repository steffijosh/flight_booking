package com.demo.flightBooking.controller;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.flightBooking.entities.Airline;
import com.demo.flightBooking.entities.Booking;
import com.demo.flightBooking.entities.Discount;
import com.demo.flightBooking.entities.EmailReceipients;
import com.demo.flightBooking.entities.Flight;
import com.demo.flightBooking.service.AdminService;
import com.demo.flightBooking.service.EmailService;

@RequestMapping("admin_Airline")
@RestController
@CrossOrigin(origins = { "http://localhost:4200" , "http://ec2-3-131-100-69.us-east-2.compute.amazonaws.com"})
public class AdminController {

	@Autowired
	private AdminService adminService;
	
	@Autowired
	private EmailService emailService;

	@Autowired
	private KafkaTemplate<String, Flight> kafkaTemplate;

	private static final String TOPIC = "blocked_airline";

	@GetMapping("/blockFlight/{airlineName}")
	public List<Flight> blockFlight(@PathVariable String airlineName) {
		Airline airline = adminService.getAirlineByName(airlineName);
		System.out.println(airline);
		List<Flight> savedFlights = new ArrayList<Flight>();
		List<Flight> ResultFlights = new ArrayList<Flight>();
		// System.out.println("Airline" + airline);
		// Flight flight = new Flight();
		List<Flight> blockedFlights = new ArrayList<Flight>();
		List<Booking> blockbooking = new ArrayList<Booking>();
		List<Long> flightIdList = new ArrayList<Long>();
		try {
			if (airline != null) {
				blockedFlights = adminService.getBlockedFlights(airline.getId());
				

				if (blockedFlights != null) {
					// blockedFlights.stream().forEach(bf -> bf.setActive(true));

					for (Flight f : blockedFlights) {
						f.setActive(true);
						System.out.println("Before sending to topic " + f.toString());
						kafkaTemplate.send(TOPIC, f);
						flightIdList.add(f.getId());

					}
					
					System.out.println("Published successfully");
					System.out.println(blockedFlights);
					savedFlights = adminService.updateBlockedFlight(blockedFlights);
					System.out.println(savedFlights);
					ResultFlights = savedFlights;
					
					
					//Add blocked flights to booking
					List<Booking> tempBooking = new ArrayList<Booking>();
					for(Long id : flightIdList) {
						
						blockbooking = adminService.getBlockedBooking(id);
						tempBooking.addAll(blockbooking);
					}
					System.out.println("blockbooking" + tempBooking);
					
				if(tempBooking != null) {
					for (Booking b : tempBooking) {
						b.setActive(true);
						System.out.println("Before sending to topic " + b.toString());
					}
					List<Booking> blockedBooking = adminService.updateBlockedBooking(tempBooking);
					
					System.out.println("blockedBooking" + blockedBooking);
					emailsendToBooker(blockedBooking);
				}
					
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			ResultFlights = null;
		}

		return ResultFlights;
	}
	
	private void emailsendToBooker(List<Booking> blockedBooking) {
		try {
	     List<EmailReceipients> bookerEmailId = new ArrayList<EmailReceipients>();
	     for(Booking b : blockedBooking) {
	    	 bookerEmailId.add(new EmailReceipients(b.getBookerName(),b.getBookerEmailId(), b.getPnr(), b.getDepartureDate()));
	     }
	     System.out.println(bookerEmailId);
	     for(EmailReceipients emailRecei : bookerEmailId) {
//	    	 String content = " Hi " + emailRecei.getName() + "," +"\n" + "\n" + "Your flight scheduled on " 
//	            +emailRecei.getDate() +" of PNR Number " + emailRecei.getPnr() + " has been cancelled. " + "\n" + "\n" 
//	    			+ "Please contact 3244353346 for support ";
	    	 
	    	 
	    	 String Htmlcontent = " Hi " + emailRecei.getName() + "," +"<br><br>" + "Your flight scheduled on " 
	 	            +emailRecei.getDate() +" of PNR Number " + emailRecei.getPnr() + " has been cancelled. " +
	    			 "<br><br>" + "Please contact 3244353346 for support .";
	    	 
	    	 String subject =  "Alert !!! " + emailRecei.getPnr() + " has been cancelled";
	    	 emailService.send("FlightSupport@noreply.com", emailRecei.getEmailId(), subject, 
	    			 Htmlcontent);
	    		System.out.println("Mail Sent successfully to " + emailRecei.getEmailId());
	 		 
	     }
	     
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error occured while sending mail");
		}
	    
	}

	@GetMapping("/unblockFlight/{id}")
	public Flight blockFlight(@PathVariable Long id) {
	Flight blockedFlight = 	adminService.getFlightByID(id);
	//System.out.println(blockedFlight);
	blockedFlight.setActive(false);
	System.out.println(blockedFlight);
	return adminService.updateFlight(blockedFlight);
	}

	@GetMapping("check")
	public String getFlight() {
		int i = 10 / 0;
		System.out.println("Exception check" + i);
		return "check working";
	}

	@GetMapping("/getAllAirline")
	public List<Airline> getAllAirline() {
		return adminService.getAllAirline();
	}

	@GetMapping("/getAirlineById/{id}")
	public Airline getAirlineById(@PathVariable Long id) {
		return adminService.getAirlineById(id);
	}

	@GetMapping("/getAirlineByName/{airlineName}")
	public Airline getByAirlineName(@PathVariable String airlineName) {
		// System.out.println(adminService.getAirlineByName(airlineName));
		return adminService.getAirlineByName(airlineName);
	}

	@GetMapping("/getByFlightId/{flightId}")
	public Flight getByFlightId(@PathVariable String flightId) {
		return adminService.getByFlightId(flightId);
	}

	@GetMapping("/fromAndToLocation")
	public List<Flight> searchByfromAndToLocation(@RequestParam("from") String from, @RequestParam("To") String To) {
		return adminService.searchByfromAndToLocation(from, To);

	}

	@GetMapping("/getAllFlights")
	public List<Flight> getAllFlights() {
		return adminService.getAllFlights();

	}

	@PostMapping("/inventory/add")
	public Airline addAirline(@RequestBody Airline airline) {
		try {

			return adminService.addAirline(airline);
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}

	}

	//@GetMapping("/getFlightByAirlineName/{airlineName}")
	@GetMapping("/getFlightByAirlineName")
	public List<Flight> getFlightByAirlineName(@RequestParam("airlineName") String airlineName) {

		Airline airline = adminService.getAirlineByName(airlineName);

		if (airline != null) {
			System.out.println(airline.getId());

		}

		return adminService.getFlightByAirlineName(airline.getId());
	}

	@PostMapping("/inventory/addFlight/{airlineName}")
	public Flight addFlight(@RequestBody Flight flight, @PathVariable String airlineName) {
		try {
			Airline airline = adminService.getAirlineByName(airlineName);
			if (airline != null) {
				flight.setAirline(airline);
			}
			System.out.println(flight);
			return adminService.addFlight(flight);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@GetMapping("/getAllAirlines")
	public List<Airline> getAllAirlines() {
		return adminService.getAllAirlines();
	}

	@GetMapping("/getAirlineByID/{id}")
	public Airline getAirlineByID(@PathVariable Long id) {

		return adminService.getAirlineByID(id);
	}

	/*
	 * @DeleteMapping("deleteAirline/{id}") public void deleteAirline(@PathVariable
	 * Long id) { System.out.println("Deleted Discount" + id);
	 * adminService.deleteDiscount(id); }
	 */

	@PutMapping("/updateAirline/{id}")
	public Airline updateAirline(@RequestBody Airline airline, @PathVariable Long id) {
		List<Flight> flightList = adminService.getFlightByAirlineName(id);
		
		airline.setId(id);
		if(flightList!=null) {
			airline.setFlight(flightList);
		}
		
		return adminService.updateAirline(airline);
	}

	@GetMapping("/getFlightByID/{id}")
	public Flight getFlightByID(@PathVariable Long id) {

		return adminService.getFlightByID(id);
	}

	@PutMapping("/inventory/updateFlight")
	public Flight updateFlight(@RequestBody Flight flight, @RequestParam("airlineName") String airlineName) {
		Flight flightTemp = new Flight();
		try {

			Airline airline = adminService.getAirlineByName(airlineName);
			if (airline != null) {
				flight.setAirline(airline);
			}
			return adminService.updateFlight(flight);

		} catch (Exception e) {
			e.printStackTrace();
			// return new ResponseEntity("Error adding airline." + e,
			// HttpStatus.BAD_REQUEST);
			return null;
		}
	}

	@PutMapping("/updateFlightById/{id}")
	public Flight updateFlightById(@RequestBody Flight flight, @PathVariable Long id) {
		try {
			flight.setId(id);
			return adminService.updateFlight(flight);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@DeleteMapping("/deleteFlight/{id}")
	public void deleteFlightById(@PathVariable Long id) {
		System.out.println("Deleted Flight" + id);
		adminService.deleteFlight(id);
	}

	@GetMapping("/discount/getAllDiscount")
	public List<Discount> getAllDiscount() {
		return adminService.getAllDiscount();
	}

	@GetMapping("/discount/getDiscountByCode/{discountCode}")
	public Discount getDiscountByCode(@PathVariable String discountCode) {
		return adminService.getDiscountByCode(discountCode);
	}

	@PutMapping("/discount/updateDiscount/{id}")
	public Discount updateDiscount(@RequestBody Discount discount, @PathVariable Long id) {
		discount.setId(id);
		return adminService.updateDiscount(discount);
	}

	@PostMapping("/discount/createDiscount")
	public Discount createDiscount(@RequestBody Discount discount) {
		return adminService.createDiscount(discount);
	}

	@GetMapping("/discount/getDiscountById/{id}")
	public Discount getDiscountByID(@PathVariable Long id) {

		return adminService.getDiscountByID(id);
	}

	@DeleteMapping("/discount/deleteDiscount/{id}")
	public void deleteDiscount(@PathVariable Long id) {
		System.out.println("Deleted Discount" + id);
		adminService.deleteDiscount(id);
	}

}
