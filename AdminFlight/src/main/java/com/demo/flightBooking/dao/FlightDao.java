package com.demo.flightBooking.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.flightBooking.entities.Flight;

@Repository
public interface FlightDao extends JpaRepository<Flight,Long>{

	public Optional<Flight> findByFlightNumber(String flightId);

	@Query(value="select * from flight f where f.from_location = ?1 and f.to_location = ?2", nativeQuery = true )
	public Iterable<Flight> findByFromAndTo(String from, String to);

	@Query(value="select * from flight f where f.airline_id = ?1", nativeQuery = true )
	public List<Flight> findByAirlineId(Long id);

}
