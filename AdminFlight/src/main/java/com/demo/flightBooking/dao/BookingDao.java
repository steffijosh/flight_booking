package com.demo.flightBooking.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.flightBooking.entities.Booking;

@Repository
public interface BookingDao extends JpaRepository<Booking, Long> {

	//@Query(value = "Select * from Booking b ,passenger p where b.pnrNumber =?1 ", nativeQuery = true)
	public Booking findByPnr(String pnr);

	public List<Booking> findByBookerEmailId(String emailId);

	@Query(value = "Select * from booking b  where b.flight_ID =?1 ", nativeQuery = true)
	public List<Booking> findByFlightId(Long id);

}
