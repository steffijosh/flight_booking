package com.demo.flightBooking.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.flightBooking.entities.Discount;

@Repository
public interface DiscountDao extends JpaRepository<Discount,Long> {

	public Discount findByDiscountCode(String discountCode);

}
