package com.demo.flightBooking.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.flightBooking.entities.Airline;

@Repository
public interface AirlineDao extends JpaRepository<Airline,Long>{
	
	public Airline findByAirlineName(String airlineName);

	
}
