package com.demo.flightBooking.entities;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Discount")
public class Discount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String discountCode;
	private BigDecimal dicount;

	public Discount() {
		
	}

	public Discount(Long id, String discountCode, BigDecimal dicount) {
		
		this.id = id;
		this.discountCode = discountCode;
		this.dicount = dicount;
	}

	@Override
	public String toString() {
		return "Discount [id=" + id + ", discountCode=" + discountCode + ", dicount=" + dicount + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public BigDecimal getDicount() {
		return dicount;
	}

	public void setDicount(BigDecimal dicount) {
		this.dicount = dicount;
	}

}
