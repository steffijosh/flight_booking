package com.demo.flightBooking.entities;

import java.util.Date;

public class EmailReceipients {

	private String name;
	private String emailId;
	private String pnr;
	private Date date;

	
	
	
	
	public EmailReceipients(String name, String emailId, String pnr, Date date) {
		super();
		this.name = name;
		this.emailId = emailId;
		this.pnr = pnr;
		this.date = date;
	}

	
	@Override
	public String toString() {
		return "EmailReceipients [name=" + name + ", emailId=" + emailId + ", pnr=" + pnr + ", date=" + date + "]";
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
