package com.demo.flightBooking.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "flight")
public class Flight {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "flightNumber")
	private String flightNumber;
	@Column(name = "fromLocation")
	private String from;
	@Column(name = "toLocation")
	private String to;
//	private Date departureDateTime;
//	private Date ReturnDateTime;
	@Column(name = "totalSeats")
	private int totalSeats;
	@Column(name = "cost")
	private int cost;
	@Column(name = "scheduledDates")
	private String scheduledDates;
	@Column(name = "mealType")
	private String mealType;
	@ManyToOne(optional = false)
	@JoinColumn(name = "airlineId", insertable = true, updatable = false)
	@JsonIgnoreProperties("flight")
	private Airline airline;
	private boolean active;

	public Flight() {

	}

	public Flight(Long id, String flightNumber, String from, String to, int totalSeats, int cost, String scheduledDates,
			String mealType, Airline airline, boolean active) {
		super();
		this.id = id;
		this.flightNumber = flightNumber;
		this.from = from;
		this.to = to;
		this.totalSeats = totalSeats;
		this.cost = cost;
		this.scheduledDates = scheduledDates;
		this.mealType = mealType;
		this.airline = airline;
		this.active = active;
	}

	
	

	public Flight(Long id, String flightNumber, String from, String to) {
		super();
		this.id = id;
		this.flightNumber = flightNumber;
		this.from = from;
		this.to = to;
	}

	

	@Override
	public String toString() {
		return "Flight [id=" + id + ", flightNumber=" + flightNumber + ", from=" + from + ", to=" + to + ", totalSeats="
				+ totalSeats + ", cost=" + cost + ", scheduledDates=" + scheduledDates + ", mealType=" + mealType
				+ ", airline=" + airline + ", active=" + active + "]";
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getScheduledDates() {
		return scheduledDates;
	}

	public void setScheduledDates(String scheduledDates) {
		this.scheduledDates = scheduledDates;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public int getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public Airline getAirline() {
		return airline;
	}

	public void setAirline(Airline airline) {
		this.airline = airline;
	}

}
