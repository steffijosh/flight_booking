package com.demo.flightBooking.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "booking")
public class Booking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "booking_id" , nullable = false, insertable = true, updatable = true, unique = true)
	private Long id;

	@Column(name = "pnrNumber", nullable = false, insertable = true, updatable = false, unique = true)
	private String pnr;
	@Column(name = "flight_ID")
	private Long flightId;
	@Column(name = "travelType")
	private String type;
	private Long price;
	@Column(name = "fromLocation")
	private String from;
	@Column(name = "toLocation")
	private String to;
	private String depatureFlight;
	private String returnFlight;
	private Date departureDate;
	private Date returnDate;
	private String bookerName;
	private String bookerEmailId;
	private String bookerContactNumber;
	private int totalSeats;
	private boolean active;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "booking_id",updatable = false,insertable = true,nullable = false)
   // @JsonIgnoreProperties("booking")
	private List<Passenger> passengers;

	public Booking() {

	}

	
	public Booking(Long id, String pnr, Long flightId, String type, Long price, String from, String to,
			String depatureFlight, String returnFlight, Date departureDate, Date returnDate, String bookerName,
			String bookerEmailId, String bookerContactNumber, int totalSeats, boolean active,
			List<Passenger> passengers) {
		super();
		this.id = id;
		this.pnr = pnr;
		this.flightId = flightId;
		this.type = type;
		this.price = price;
		this.from = from;
		this.to = to;
		this.depatureFlight = depatureFlight;
		this.returnFlight = returnFlight;
		this.departureDate = departureDate;
		this.returnDate = returnDate;
		this.bookerName = bookerName;
		this.bookerEmailId = bookerEmailId;
		this.bookerContactNumber = bookerContactNumber;
		this.totalSeats = totalSeats;
		this.active = active;
		this.passengers = passengers;
	}


	public String getBookerContactNumber() {
		return bookerContactNumber;
	}

	public void setBookerContactNumber(String bookerContactNumber) {
		this.bookerContactNumber = bookerContactNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	

	public Long getFlightId() {
		return flightId;
	}

	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}


	public String getDepatureFlight() {
		return depatureFlight;
	}

	public void setDepatureFlight(String depatureFlight) {
		this.depatureFlight = depatureFlight;
	}

	public String getReturnFlight() {
		return returnFlight;
	}

	public void setReturnFlight(String returnFlight) {
		this.returnFlight = returnFlight;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getBookerName() {
		return bookerName;
	}

	public void setBookerName(String bookerName) {
		this.bookerName = bookerName;
	}

	public String getBookerEmailId() {
		return bookerEmailId;
	}

	public void setBookerEmailId(String bookerEmailId) {
		this.bookerEmailId = bookerEmailId;
	}

	public int getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

}
