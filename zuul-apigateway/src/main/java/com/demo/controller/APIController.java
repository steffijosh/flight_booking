package com.demo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = { "http://localhost:4200" , "http://ec2-3-131-100-69.us-east-2.compute.amazonaws.com"})
public class APIController {
	
	@GetMapping("/getValue")
	public String getvalue() {
		return "Hello";
	}

}