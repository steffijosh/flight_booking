package com.demo.flightBooking.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.flightBooking.entities.Passenger;

@Repository
public interface PassengerDao extends JpaRepository<Passenger, Long> {

	//public List<Passenger> findByBooking(Booking pnr);

	@Query(value = "Select * from passenger p where p.booking_id =?1" , nativeQuery = true)
	public List<Passenger> findByPnr(Long pnr);
	


}
