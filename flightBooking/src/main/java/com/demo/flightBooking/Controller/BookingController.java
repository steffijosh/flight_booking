package com.demo.flightBooking.Controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.demo.flightBooking.Exception.BookingException;
import com.demo.flightBooking.Service.BookingService;
import com.demo.flightBooking.entities.Booking;
import com.demo.flightBooking.entities.Passenger;

@RequestMapping("/booking")
@RestController
@CrossOrigin(origins = { "http://localhost:4200" , "http://ec2-3-131-100-69.us-east-2.compute.amazonaws.com"})
public class BookingController {

	@Autowired
	private BookingService bookingService;

	@Autowired
	private RestTemplate template;

	@GetMapping("/getFlights")
	public List<Object> getFlights() throws BookingException {
		try {
			System.out.println("Fetching all Flights from Airline service");
			ResponseEntity<List<Object>> res = template.exchange("http://AIRLINESERVICE/admin_Airline/getAllFlights",
					HttpMethod.GET, null, new ParameterizedTypeReference<List<Object>>() {
					});
			return res.getBody();
		} catch (Exception e) {
			throw new BookingException(e);
		}
	}

	@GetMapping("testException")
	public String exc() throws BookingException {
		try {
			int i = 1 / 0;
			return "1" + i;
		} catch (Exception e) {
			throw new BookingException(e);
		}
	}

	@GetMapping("/check")
	public Long check() {
		String s = UUID.randomUUID().toString();
		long first14 = (long) (Math.random() * 100000000000000L);
		long number = 5200000000000000L + first14;
		return number;
	}

	@GetMapping("/searchByPnr/{pnr}")
	public Booking getValuesByPNR(@PathVariable String pnr) throws BookingException {
		try {
			Booking booking = bookingService.getValuesByPNR(pnr);
			List<Passenger> listPassenger = bookingService.getPassengerValuesByPNR(booking.getId());
			booking.setPassengers(listPassenger);
			return booking;
		} catch (Exception e) {
			throw new BookingException(e);
		}

	}

	@GetMapping("/searchByEmailId/{emailId}")
	public List<Booking> getValuesByEmailId(@PathVariable String emailId) throws BookingException {
		try {
		List<Booking> booking = bookingService.getValuesByemailID(emailId);
		List<Passenger> listPassenger ;
		Passenger p = new Passenger();
		for(Booking b : booking) {
			
		listPassenger = bookingService.getPassengerValuesByPNR(b.getId());
		b.setPassengers(listPassenger);
		}
		 
		//booking.setPassengers(listPassenger);
		return booking;
		} catch (Exception e) {
			throw new BookingException(e);
		}
	}

	@PostMapping("/bookTicket")
	public Booking bookTicket(@RequestBody Booking booking) {
		Booking savedBooking;
		if (booking != null) {
			String pnr = generatePnr();
			booking.setPnr(pnr);
			savedBooking = bookingService.saveBooking(booking);
		} else {
			savedBooking = null;
		}

		return savedBooking;
	}

	@DeleteMapping("/cancelTicket/{id}")
	public void cancelTicket(@PathVariable Long id) {

		bookingService.deleteTicket(id);
	}

	private String generatePnr() {
		long first14 = (long) (Math.random() * 100000000000000L);
		long number = 5200000000000000L + first14;
		String s = String.valueOf(number);
		return s;
	}

}
