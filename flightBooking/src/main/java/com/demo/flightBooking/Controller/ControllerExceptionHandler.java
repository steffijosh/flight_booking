package com.demo.flightBooking.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.demo.flightBooking.Exception.BookingException;


@ControllerAdvice
public class ControllerExceptionHandler{
	
	@ExceptionHandler(BookingException.class)
	public ResponseEntity<String> handleException(Exception e){
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("error");
	}

	
}
