package com.demo.flightBooking.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import com.demo.flightBooking.Exception.BookingException;
import com.demo.flightBooking.dao.BookingDao;
import com.demo.flightBooking.dao.PassengerDao;
import com.demo.flightBooking.entities.Booking;
import com.demo.flightBooking.entities.Flight;
import com.demo.flightBooking.entities.Passenger;

@Service
public class BookingServiceImpl implements BookingService {

	@Autowired
	private BookingDao bookingDao;

	@Autowired
	private PassengerDao passengerDao;

	@KafkaListener(topics = "blocked_airline", groupId = "group_id", containerFactory = "userKafkaListenerFactory")
	public void consumeJson(Flight Flight) {
		System.out.println("Consumed JSON Message: " + Flight);
		System.out.println("Blocked List of flight number sent" + Flight.getFlightNumber());
	}

	@Override
	public Booking getValuesByPNR(String pnr) {
		// TODO Auto-generated method stub
		return bookingDao.findByPnr(pnr);
	}

	@Override
	public List<Passenger> getPassengerValuesByPNR(Long id) {
		// TODO Auto-generated method stub
		return passengerDao.findByPnr(id);
	}

	@Override
	public List<Booking> getValuesByemailID(String emailId) {
		// TODO Auto-generated method stub
		return bookingDao.findByBookerEmailId(emailId);
	}

	@Override
	public Booking saveBooking(Booking booking) {
		// TODO Auto-generated method stub
		return bookingDao.save(booking);
	}

	@Override
	public void deleteTicket(Long id) {
		bookingDao.deleteById(id);

	}

}
