package com.demo.flightBooking.Service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;


import com.demo.flightBooking.entities.Booking;
import com.demo.flightBooking.entities.Passenger;

public interface BookingService {

	
	Booking getValuesByPNR(String pnr);

	List<Passenger> getPassengerValuesByPNR(Long long1);

	List<Booking> getValuesByemailID(String emailId);

	Booking saveBooking(Booking booking);

	void deleteTicket(Long id);

}
