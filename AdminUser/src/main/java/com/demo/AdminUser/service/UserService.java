package com.demo.AdminUser.service;

import com.demo.AdminUser.entities.User;

public interface UserService {

	User registerUser(User user);

}
