package com.demo.AdminUser.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.demo.AdminUser.dao.UserDao;
import com.demo.AdminUser.entities.User;

@Service
public class UserServiceImpl  implements UserService{
	
	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	private UserDao userDao;

	@Override
	public User registerUser(User user) {
		// TODO Auto-generated method stub
		user.setPassword(encoder.encode(user.getPassword()));
		System.out.println("encoded password :" + encoder.encode(user.getPassword()));
		return userDao.save(user);
	}

}
