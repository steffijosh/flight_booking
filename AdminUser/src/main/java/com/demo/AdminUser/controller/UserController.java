package com.demo.AdminUser.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.AdminUser.entities.User;
import com.demo.AdminUser.service.UserService;

@RequestMapping("/v1/api/flight/admin")
@RestController
@CrossOrigin(origins = { "http://localhost:4200" , "http://ec2-3-131-100-69.us-east-2.compute.amazonaws.com"})
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/user")
	public String username() {
		return "hi admin";
	}
	
	@PostMapping("/register")
	public User registerUser(@RequestBody User user) {
		System.out.println("User: " + user);
		return userService.registerUser(user);
	}

}
