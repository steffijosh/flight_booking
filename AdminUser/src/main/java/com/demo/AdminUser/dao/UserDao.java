package com.demo.AdminUser.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.AdminUser.entities.User;

@Repository
public interface UserDao extends JpaRepository<User, Long>{

}
